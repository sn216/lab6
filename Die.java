import java.util.Random;

public class Die 
{
    Random rand = new Random();

    private int faceValue;
    private int randomValue = rand.nextInt(6) + 1;

    public Die ()
    {
        faceValue = 1;
    }

    public int getFaceValue()
    {
        return this.faceValue;
    }

    public int roll()
    {
        return faceValue = randomValue;
    }

    public String toString()
    {
        String s = String.valueOf(faceValue);
        return s;
    }    
}
