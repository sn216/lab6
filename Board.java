public class Board 
{
    private Die firstDie;
    private Die secondDie;
    private boolean[] tiles;

    public Board()
    {
        tiles = new boolean[12];
        this.firstDie = new Die();
        this.secondDie = new Die();
    }

    @Override
    public String toString() 
    {
        String s = "";
        for(int i=0; i < this.tiles.length;i++)
        {
            if(!tiles[i])
            {
                s += i +1;
            }

            else
            {
                s += "X";
            }

            if(i < tiles.length-1)
            {
                s += ",";
            }
        }
        return s;
    }

    public boolean playATurn()
    {
        firstDie.roll();
        secondDie.roll();

        System.out.println( "First Die: " + firstDie.getFaceValue());
        System.out.println("Second Die: " + secondDie.getFaceValue());

        int sumOfDice = firstDie.getFaceValue() + secondDie.getFaceValue();
        if(!tiles[sumOfDice - 1])
        {
            tiles[sumOfDice - 1] = true;
            System.out.println( "Closing tile equal to sum: " + tiles[sumOfDice - 1]);
            return false;
        }

        else if(!tiles[firstDie.getFaceValue() -1])
        {
            tiles[firstDie.getFaceValue() -1] = true;
            System.out.println("Closing tile with the same value as die one: " + firstDie.getFaceValue());
            return false;
        }

        else if(!tiles[secondDie.getFaceValue() -1])
        {
            tiles[secondDie.getFaceValue() -1] = true;
            System.out.println("Closing tile with the same value as die two: " + secondDie.getFaceValue());
            return false;
        }

        else{
            System.out.println("All the tiles for these values are already shut");
            return true;
        }

    }
}
