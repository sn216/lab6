public class Jackpot {
    public static void main (String[]args)
    {
        System.out.println("Hello There!");

        Board gameBoard = new Board();
        boolean gameOver = false;
        int numOfTilesClosed = 0;

        while (!gameOver)
        {
            System.out.println(gameBoard);

            if(gameBoard.playATurn() == true)
            {
                gameOver = true;
            }
            else{
                numOfTilesClosed++;
            }
        }

        if (numOfTilesClosed >= 7)
        {
            System.out.println("You won the jackpot!");
        }

        else{
            System.out.println("You lost");
        }
    }

    
}

